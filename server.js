// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

var mongoose   = require('mongoose');
mongoose.connect('localhost:27017'); // connect to our database

var Quote     = require('./app/models/quote');

// OTHER UTILS
var util = require('util');

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.Request arrived!');
	// Can use util.inspect(req, false, null) to view full content of the object.
	next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'Hooray! welcome to Quotes api!' });   
});

// more routes for our API will happen here

// on routes that end in /quotes
// ----------------------------------------------------
router.route('/quotes')
    // create a quote (accessed at POST http://localhost:8080/api/quotes)
    .post(function(req, res) {        
        var quote = new Quote();    // create a new instance of the Quote model
		
        quote.quote = req.body.quote;
		quote.author = req.body.author;
		quote.tags = req.body.tags.split(",").map(function(str) {return str.trim()});

        // save the quote and check for errors
        quote.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'Quote created!' });
        });
    })
	
	// get all the quotes (accessed at GET http://localhost:8080/api/quotes)
	.get(function(req, res) {
		Quote.find(function(err, quotes) {
		  if (err)
		    res.send(err);
		  res.json(quotes);
		});
    });

// on routes that end in /quotes/:quote_id
// ----------------------------------------------------
router.route('/quotes/:quote_id')
    // get the quote with that id (accessed at GET http://localhost:8080/api/quotes/:quote_id)
    .get(function(req, res) {
        Quote.findById(req.params.quote_id, function(err, quote) {
            if (err)
                res.send(err);
            res.json(quote);
        });
    })
	
	// update the quote with this id (accessed at PUT http://localhost:8080/api/quotes/:quote_id)
	.put(function(req, res) {
	    // use our quote model to find the quote we want
	    Quote.findById(req.params.quote_id, function(err, quote) {
          if (err)
            res.send(err);
        
          quote.quote = req.body.quote;
		  quote.author = req.body.author;
		  quote.tags = req.body.tags.split(",").map(function(str) {return str.trim()});
		
          // save the quote
	      quote.save(function(err) {
	        if (err)
	          res.send(err);
            res.json({ message: 'Quote updated!' });
	      });
	    })
    })
	 
	// delete the bear with this id (accessed at DELETE http://localhost:8080/api/quotes/:quote_id)
	.delete(function(req, res) {
	   Quote.remove({
	       _id: req.params.quote_id
	     }, 
		 function(err, quote) {
	       if (err)
	           res.send(err);
	       res.json({ message: 'Successfully deleted' });
         });
     });

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);